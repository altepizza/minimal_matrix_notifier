# Minimal Matrix Notifier

I simply didn't find any simple and easy to use script to send messages into
Matrix rooms.

## Installation

1. Copy the `config.ini.template` into `config.ini`
2. fill in the missing details in the `config.ini` file

## Usage

# For SSH login notifications

`--ssh` will send general information about the current ssh session. Can be
used for example as part of a shell script

# For sending text

`--text <SOME TEXT>` will send `<SOME TEXT>`
