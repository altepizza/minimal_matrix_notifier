import requests
import uuid
import configparser
import os
import socket
import getpass
import argparse


# Parsing configs

config = configparser.ConfigParser()
config.read(os.path.dirname(os.path.realpath(__file__)) + '/config.ini')
HOME_SERVER = config.get('MATRIX', 'home_sever')
USER = config.get('MATRIX', 'user')
PASS = config.get('MATRIX', 'password')
ROOM_ID = config.get('MATRIX', 'room_id')


# Setting up constants

API_ENDPOINT_TOKEN = "/client/r0/login"
API_ENDPOINT_INVITE = "/client/r0/rooms/" + ROOM_ID + "/join"
API_ENDPOINT_SEND_MESSAGE = "/client/r0/rooms/" + ROOM_ID
API_ENDPOINT_SEND_MESSAGE += "/send/m.room.message/" + str(uuid.uuid4())

# Parsing args

parser = argparse.ArgumentParser()
parser.add_argument('--ssh', help='send infos brecause of ssh login',
                    action='store_true')
parser.add_argument('--text', type=str, help='custom text to be send')
args = parser.parse_args()


# Getting access token

body = {"type": "m.login.password",
        "identifier": {
            "type": "m.id.user", "user": USER
        },
        "password": PASS}
resp = requests.post(HOME_SERVER + API_ENDPOINT_TOKEN, json=body)

if resp.status_code != requests.codes.ok:
    resp.raise_for_status()
access_token = resp.json()['access_token']


# Joining room if needed

headers = {"Authorization": "Bearer " + access_token}
resp = requests.post(HOME_SERVER + API_ENDPOINT_INVITE, headers=headers)
if resp.status_code != requests.codes.ok:
    resp.raise_for_status()


# Create and sending text

if args.ssh:
    text = "Login on " + socket.gethostname() + " by " + getpass.getuser()
    ssh = os.getenv('SSH_CONNECTION') or ''
    if ssh:
        text += ' (' + ssh + ')'

if args.text:
    text = args.text

body = {"msgtype": "m.text",
        "body": text}
resp = requests.put(HOME_SERVER + API_ENDPOINT_SEND_MESSAGE, headers=headers,
                    json=body)
if resp.status_code != requests.codes.ok:
    resp.raise_for_status()
